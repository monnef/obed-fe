open Data;

type action =
  | PlansLoaded(array(plan));

type state = {plans: option(array(plan))};

[@react.component]
let make = () => {
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | PlansLoaded(plans) => {plans: plans->Js.Option.some}
        },
      {plans: None},
    );

  let loadPlans = () => Api.(getPlans() |> then_(resp => PlansLoaded(resp.data) |> dispatch |> resolve) |> ignore);

  if (state.plans->Js.Option.isNone) {
    loadPlans();
  };

  let plansPart =
    switch (state.plans) {
    | Some(plans) =>
      plans
      |> Array.map(p => <PlanView plan=p key={p.id |> Js.Option.getWithDefault(-1) |> string_of_int} />)
      |> React.array
    | _ => <div> {React.string("Loading ...")} </div>
    };

  <div> plansPart </div>;
};
