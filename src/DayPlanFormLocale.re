let pageLocale =
  [@intl.messages]
  {
    "fieldDateLabel": {
      "defaultMessage": "Date",
      "id": "dayPlanForm.field.date.label",
    },
    "fieldDateLabelErrorMissing": {
      "defaultMessage": "Missing date",
      "id": "dayPlanForm.field.date.error.missing",
    },
    "fieldDateLabelErrorInvalidFormat": {
      "defaultMessage": "Invalid date format",
      "id": "dayPlanForm.field.date.error.invalidFormat",
    },
    "fieldCaptionLabel": {
      "defaultMessage": "Meal",
      "id": "dayPlanForm.field.caption.label",
    },
    "fieldCaptionErrorMissing": {
      "defaultMessage": "Missing caption",
      "id": "dayPlanForm.field.caption.error.missing",
    },
    "fieldSmallNoteLabel": {
      "defaultMessage": "Small note",
      "id": "dayPlanForm.field.smallNote.label",
    },
    "fieldNoteLabel": {
      "defaultMessage": "Note",
      "id": "dayPlanForm.field.note.label",
    },
    "fieldNewItemLabel": {
      "defaultMessage": "New item",
      "id": "dayPlanForm.field.newItem.label",
    },
  };
