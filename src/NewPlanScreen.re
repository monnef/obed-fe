open Data;
open Utils;
open Js.Promise;
open ReactIntl;
open NewPlanScreenLocale;
open Api;

module Styles = {
  open Css;
  open MainStyles;

  let itemsWrapper =
    merge([Decoration.indentedBox, style([marginBottom(em(3.0))])]);
};

type dayPlanState =
  | DpFormHidden
  | NewDp
  | EditingDp;

type action =
  | AddDayPlan(dayPlan)
  | ModifyDayPlan(int, dayPlan)
  | ChangeDpFormState(dayPlanState)
  | RemoveDayPlan(int);

type state = {
  plan,
  dayPlanState,
};

let emptyPlan: plan = {items: [||], id: None};

[@react.component]
let make = (~authData: option(authInfo)) => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | AddDayPlan(dp) => {
            ...state,
            plan: {
              ...state.plan,
              items: state.plan.items->Array.append([|dp|]),
            },
          }
        | ModifyDayPlan(idx, dp) => {
            ...state,
            plan: {
              ...state.plan,
              items: state.plan.items->setIdx(idx, dp),
            },
          }
        | RemoveDayPlan(idx) => {
            ...state,
            plan: {
              ...state.plan,
              items: state.plan.items->arrWithoutIdx(idx),
            },
          }
        | ChangeDpFormState(newS) => {...state, dayPlanState: newS}
        },
      {plan: emptyPlan, dayPlanState: DpFormHidden},
    );
  let dialog = ModalDialog.useDialog();
  let t = ReactIntl.useIntl();

  let handleDayPlanFormSubmit = (s: DayPlanForm.DayPlanForm.state) => {
    Js.log2("handleDayPlanFormSubmit", s);
    let dp: dayPlan = {
      id: None,
      caption: s.caption,
      date: s.date |> parseDate |> formatBeDate,
      planId: None,
      toBuyList: s.toBuyList,
      note: s.note |> optEmptyStr,
      smallNote: s.smallNote |> optEmptyStr,
    };
    dispatch(AddDayPlan(dp));
    dispatch(ChangeDpFormState(DpFormHidden));
  };
  let handleDayPlanFormCancel = () => {
    dispatch(ChangeDpFormState(DpFormHidden));
  };
  let handleNewDayPlanClick = () => {
    dispatch(ChangeDpFormState(NewDp));
  };
  // TODO: if any day plan is present, then it should be day after that day plan
  let defaultDate =
    MomentRe.(
      momentNow()
      |> Moment.add(
           ~duration=
             duration(
               1.0 +. (state.plan.items |> Array.length |> float_of_int),
               `days,
             ),
         )
      |> formatDate
    );
  let handleCreatePlanClicked = () => {
    dialog.show(
      Intl.formatMessage(t, pageLocale##createPlanConfirmation),
      ModalDialogTypes.Confirm,
      button =>
      if (button == ModalDialogTypes.OkButton) {
        Js.log("handleCreatePlanClicked");
        Api.createPlan(state.plan, authData->Belt.Option.getExn)
        |> then_(_resp => {ReasonReactRouter.push("/plans") |> resolve})
        |> catch(err => {
             Js.Console.error2("Failed to create a plan", err) |> resolve
           })
        |> ignore;
      }
    );
  };
  let handleDayPlanDelete = (i: int) => {
    dialog.show(
      Intl.formatMessageWithValues(
        t,
        pageLocale##deletePlanConfirmation,
        {"caption": state.plan.items->Array.get(i).caption},
      ),
      ModalDialogTypes.Confirm,
      button =>
      if (button == ModalDialogTypes.OkButton) {
        dispatch(RemoveDayPlan(i));
      }
    );
  };
  let content =
    <div>
      <h2>
        <FormattedMessage
          id="newPlanScreen.caption"
          defaultMessage="New plan"
        />
      </h2>
      <div className=Styles.itemsWrapper>
        {state.plan.items
         |> Array.mapi((i, dp) =>
              <DayPlanView
                dayPlan=dp
                key={i->string_of_int}
                showDeleteButton=true
                onDelete={_dp => handleDayPlanDelete(i)}
              />
            )
         |> rArr}
        {state.plan.items->Js.Array.length <= 0
           ? <div>
               <FormattedMessage
                 id="newPlanScreen.noDayPlans"
                 defaultMessage="No day plans"
               />
             </div>
           : rNull}
        <br />
        {state.dayPlanState !== DpFormHidden
           ? <DayPlanForm
               onSubmit=handleDayPlanFormSubmit
               onCancel=handleDayPlanFormCancel
               defaultDate
             />
           : <button onClick={_evt => handleNewDayPlanClick()}>
               <FormattedMessage
                 id="newPlanScreen.addDayPlan"
                 defaultMessage="Add day plan"
               />
             </button>}
      </div>
      <button
        onClick={_evt => handleCreatePlanClicked()}
        disabled={
          state.plan.items->arrEmpty || state.dayPlanState !== DpFormHidden
        }>
        <FormattedMessage
          id="newPlanScreen.createPlan"
          defaultMessage="Create plan"
        />
      </button>
    </div>;
  switch (authData) {
  | Some(_) => content
  | None =>
    <FormattedMessage
      id="common.pleaseLogIn"
      defaultMessage="Please log in first."
    />
  };
};
