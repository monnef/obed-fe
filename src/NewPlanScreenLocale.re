let pageLocale =
  [@intl.messages]
  {
    "createPlanConfirmation": {
      "defaultMessage": "Create plan? (Operation is not reversible, edit nor delete isn't implemented yet.)",
      "id": "newPlanScreen.createPlanConfirmation",
    },
    "deletePlanConfirmation": {
      "defaultMessage": "Really remove day plan \"{caption}\"?",
      "id": "newPlanScreen.deletePlanConfirmation",
    },
  };
