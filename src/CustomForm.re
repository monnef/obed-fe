open Utils;

module Styles = {
  open Css;
  open MainStyles;

  let input = merge([inputBase, style([backgroundColor(cyan)])]);
  let error = style([color(Colors.error)]);
};

module type CustomForm = {
  include Formality__Form.Form;
  let messageToIntlMessage: message => ReactIntl.message;
};

let preventKeyPressEnter = (evt: ReactEvent.Keyboard.t): unit =>
  if (ReactEvent.Keyboard.key(evt) == "Enter") {
    ReactEvent.Synthetic.preventDefault(evt);
  };

type textInputType =
  | TextInputText
  | TextInputPassword;

let titToStr = (x: textInputType) =>
  switch (x) {
  | TextInputText => "text"
  | TextInputPassword => "password"
  };

module Make = (F: CustomForm) => {
  module FormHook = Formality.Make(F);
  include FormHook;
  open Formality;
  type formType = FormHook.interface;
  type useReturn = {
    form: formType,
    renderTextInput:
      (
        F.field,
        F.state => string,
        (F.state, string) => F.state,
        string,
        ~onKeyPress: ReactEvent.Keyboard.t => unit=?,
        ~type_: textInputType=?,
        unit
      ) =>
      React.element,
  };

  let renderError = (form: formType, f: F.field, t: ReactIntl.Intl.t) =>
    switch (form.result(f)) {
    | Some(Error(message)) =>
      <div className=Styles.error>
        {F.messageToIntlMessage(message)
         |> ReactIntl.Intl.formatMessage(t)
         |> rStr}
      </div>
    | Some(Ok(Valid | NoValue))
    | None => React.null
    };

  let changeHandler:
    (
      formType,
      F.field,
      (F.state, string) => F.state,
      ReactEvent.synthetic(ReactEvent.Form.tag)
    ) =>
    unit =
    (form, fieldName, fieldModUpdate, evt) =>
      form.change(
        fieldName,
        fieldModUpdate(form.state, evt->ReactEvent.Form.target##value),
      );

  let renderTextInput:
    (
      formType,
      ReactIntl.Intl.t,
      F.field,
      F.state => string,
      (F.state, string) => F.state,
      string,
      ~onKeyPress: ReactEvent.Keyboard.t => unit=?,
      ~type_: textInputType=?,
      unit
    ) =>
    React.element =
    (
      form: formType,
      t: ReactIntl.Intl.t,
      field: F.field,
      getter: F.state => string,
      setter: (F.state, string) => F.state,
      label: string,
      ~onKeyPress=noop,
      ~type_=TextInputText,
      (),
    ) =>
      <>
        <input
          value={getter(form.state)}
          disabled={form.submitting}
          onBlur={_ => form.blur(field)}
          onChange={e => changeHandler(form, field, setter, e)}
          placeholder=label
          className=Styles.input
          onKeyDown=onKeyPress
          type_={type_->titToStr}
        />
        {renderError(form, field, t)}
      </>;

  let useFormCustom =
      (
        ~initialState: F.state,
        ~onSubmit:
           (
             F.state,
             Formality__Validation.submissionCallbacks(
               F.state,
               F.submissionError,
             )
           ) =>
           unit,
        ~t: ReactIntl.Intl.t,
      )
      : useReturn => {
    let form = FormHook.useForm(~initialState, ~onSubmit);
    let rti = renderTextInput(form, t);
    {form, renderTextInput: rti};
  };
};
