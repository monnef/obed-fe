open Data;
open Utils;

module Styles = {
  open Css;

  let wrapper = style([]);
};

[@react.component]
let make = (~plan: plan) => {
  <div className=Styles.wrapper>
    <h1> {rStr("Plan " ++ (plan.id->Belt.Option.map(string_of_int) |> Js.Option.getWithDefault("?")))} </h1>
    {plan.items |> Array.mapi((i, dp) => <DayPlanView dayPlan=dp key={i->string_of_int} />) |> React.array}
  </div>;
};
