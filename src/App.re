open Utils;
open Language;
open Data;
open ReactIntl;

type action =
  | SetLanguage(language)
  | SetUser(option(loggedInUser))
  | SetAuthData(option(Api.authInfo))
  | MarkInitialUserFetch;

type state = {
  language,
  user: option(loggedInUser),
  initialUserFetchDone: bool,
  authData: option(Api.authInfo),
};

let browserLangStr: string = [%bs.raw {|navigator.language.split(/[-_]/)[0]|}];
let defaultLanguage =
  strToLang(browserLangStr)->Belt.Option.getWithDefault(En);

[@react.component]
let make = () => {
  let url = ReasonReactRouter.useUrl();
  let (state, dispatch) =
    React.useReducer(
      (st, action) =>
        switch (action) {
        | SetLanguage(l) => {...st, language: l}
        | SetUser(u) => {...st, user: u}
        | MarkInitialUserFetch => {...st, initialUserFetchDone: true}
        | SetAuthData(a) => {...st, authData: a}
        },
      {
        language: defaultLanguage,
        user: None,
        initialUserFetchDone: false,
        authData: None,
      },
    );

  let onLoginSuccess = (userInfo: loggedInUser) => {
    Js.log2("onLoginSuccess", userInfo);
    SetUser(Some(userInfo)) |> dispatch;
  };

  let fetchUser = (a: option(Api.authInfo)) => {
    switch (a) {
    | None => ()
    | Some(a) =>
      Js.log2("fetchUser", a);
      Api.(
        getUser(a)
        |> Js.Promise.then_(resp => onLoginSuccess(resp.data) |> resolve)
        |> ignore
      );
    };
  };

  if (!state.initialUserFetchDone) {
    fetchUser(state.authData);
    dispatch(MarkInitialUserFetch);
  };

  let handleLoginSubmit = auth => {
    dispatch(SetAuthData(Some(auth)));
    fetchUser(Some(auth));
  };

  let content =
    switch (url.path) {
    | ["new-plan"] => <NewPlanScreen authData={state.authData} />
    | ["plan", id] => <PlanScreen id />
    | ["plans"] => <PlansListScreen />
    | ["test"] => <TestScreen />
    | ["login"] =>
      <LoginScreen user={state.user} onFormFilled=handleLoginSubmit />
    | [] => <RootScreen />
    | x =>
      Js.Console.log2("unknown route", x);
      <>
        <FormattedMessage id="app.noRoute" defaultMessage="URL not found" />
        <br />
        {x |> Array.of_list |> Js.Array.joinWith("/") |> rStr}
      </>;
    };

  let allTranslations = Js.Dict.fromList([("cs", TranslationsCs.data)]);
  let messages =
    Js.Dict.get(allTranslations, state.language->langToStr)
    ->Belt.Option.getWithDefault(Js.Dict.fromList([]));

  <>
    <ReactIntl.IntlProvider locale={langToStr(state.language)} messages>
      <ModalDialog>
        <MainMenu
          onChangeLanguage={x => dispatch(SetLanguage(x))}
          activeLanguage={state.language}
          availableLanguages=[|En, Cs|]
          user={state.user}
        />
        content
        <Footer />
      </ModalDialog>
    </ReactIntl.IntlProvider>
  </>;
};
