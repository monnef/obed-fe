open Utils;

[@react.component]
let make = () => {
  let ctx = React.useContext(ModalDialogContextProvider.modalDialogContext);
  let dialog = ModalDialog.useDialog();
  <div>
    <h1> {rStr("Dialog test")} </h1>
    <h2> {rStr("direct ctx")} </h2>
    <button
      onClick={_e => {ctx.show("Text from ROOT!", ModalDialogTypes.Info, b => Js.Console.log2("root handler", b))}}>
      {rStr("info")}
    </button>
    <button
      onClick={_e => {
        ctx.show("Text #2 from ROOT!", ModalDialogTypes.Confirm, b => Js.Console.log2("root handler 2", b))
      }}>
      {rStr("confirm")}
    </button>
    <h2> {rStr("hook")} </h2>
    <button
      onClick={_e => {
        dialog.show("Text from ROOT! Hook", ModalDialogTypes.Info, b => Js.Console.log2("root handler", b))
      }}>
      {rStr("info")}
    </button>
    <button
      onClick={_e => {
        dialog.show("Text #2 from ROOT! Hook", ModalDialogTypes.Confirm, b => Js.Console.log2("root handler 2", b))
      }}>
      {rStr("confirm")}
    </button>
  </div>;
};
