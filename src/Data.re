type toBuyListItem = {
  id: option(int),
  value: string,
  dayPlanId: option(int),
};

let decodeToBuyListItem = json =>
  Json.Decode.{
    value: json |> field("value", string),
    id: json |> optional(field("id", int)),
    dayPlanId: json |> optional(field("dayPlanId", int)),
  };

let encodeToBuyListItem = (c: toBuyListItem) =>
  Json.Encode.(
    object_([
      ("value", string(c.value)),
      ("id", c.id |> nullable(int)),
      ("dayPlanId", c.dayPlanId |> nullable(int)),
    ])
  );

type dayPlan = {
  id: option(int),
  caption: string,
  date: string,
  planId: option(int),
  toBuyList: array(toBuyListItem),
  note: option(string),
  smallNote: option(string),
};

let decodeDayPlan = json =>
  Json.Decode.{
    id: json |> optional(field("id", int)),
    caption: json |> field("caption", string),
    date: json |> field("date", string),
    planId: json |> optional(field("planId", int)),
    toBuyList: json |> field("toBuyList", array(decodeToBuyListItem)),
    note: json |> optional(field("note", string)),
    smallNote: json |> optional(field("smallNote", string)),
  };

let encodeDayPlan = (c: dayPlan) =>
  Json.Encode.(
    object_([
      ("id", c.id |> nullable(int)),
      ("caption", c.caption |> string),
      ("date", c.date |> string),
      ("planId", c.planId |> nullable(int)),
      ("toBuyList", c.toBuyList |> array(encodeToBuyListItem)),
      ("note", c.note |> nullable(string)),
      ("smallNote", c.smallNote |> nullable(string)),
    ])
  );

type plan = {
  id: option(int),
  items: array(dayPlan),
};

let decodePlan = json =>
  Json.Decode.{
    id: json |> optional(field("id", int)),
    items: json |> field("items", array(decodeDayPlan)),
  };

let encodePlan = (c: plan) =>
  Json.Encode.(
    object_([
      ("id", c.id |> nullable(int)),
      ("items", c.items |> array(encodeDayPlan)),
    ])
  );

let decodePlans = json => Json.Decode.(json |> array(decodePlan));

type loggedInUser = {name: string};

let decodeLoggedInUser = json =>
  Json.Decode.{name: json |> field("name", string)};

let encodeLoggedInUser = (c: loggedInUser) =>
  Json.Encode.(object_([("name", c.name |> string)]));
