open Utils;
open ModalDialogTypes;

module Styles = {
  open Css;
  open MainStyles;

  let wrapper =
    style([
      display(`flex),
      flexDirection(column),
      alignItems(center),
      justifyContent(center),
      position(fixed),
      backgroundColor(rgba(0, 0, 0, 0.33)),
      top(zero),
      left(zero),
      width(pct(100.0)),
      height(pct(100.0)),
    ]);
  let window =
    style([
      border(px(5), solid, Colors.bgDark),
      backgroundColor(Colors.bgPrimary),
      borderRadius(em(0.5)),
      borderWidth(em(0.2)),
      width(em(20.0)),
      padding2(~v=em(1.0), ~h=em(1.5)),
    ]);
  let buttons = style([display(`flex), marginTop(em(1.0)), justifyContent(spaceBetween)]);
};

type state = {
  shown: bool,
  text: string,
  mode: dialogMode,
  callback: callbackT,
};

type action =
  | Show(string, dialogMode, callbackT)
  | Hide;

let renderDialog = (state, clickHandler) => {
  let buttons =
    switch (state.mode) {
    | Confirm =>
      <>
        <button onClick={_ => clickHandler(CancelButton)}> {rStr("Cancel")} </button>
        <button onClick={_ => clickHandler(OkButton)}> {rStr("Confirm")} </button>
      </>
    | Info => <> <span /> <button onClick={_ => clickHandler(OkButton)}> {rStr("Ok")} </button> </>
    };
  <div className=Styles.wrapper>
    <div className=Styles.window> <div> {rStr(state.text)} </div> <div className=Styles.buttons> buttons </div> </div>
  </div>;
};

[@react.component]
let make = (~children) => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | Show(text, mode, callback) => {shown: true, text, mode, callback}
        | Hide => {...state, shown: false}
        },
      {shown: false, text: "", mode: Confirm, callback: _ => ()},
    );
  let buttonClickHandler = b => {
    dispatch(Hide);
    state.callback(b);
  };
  let dialogEl = state.shown ? renderDialog(state, buttonClickHandler) : rNull;
  let ctx = {shown: true, show: (text, typ, cb) => Show(text, typ, cb) |> dispatch, hide: () => dispatch(Hide)};
  <ModalDialogContextProvider value=ctx> children dialogEl </ModalDialogContextProvider>;
};

let useDialog = (): modalDialogContextT => {
  let ctx = React.useContext(ModalDialogContextProvider.modalDialogContext);
  ctx;
};
