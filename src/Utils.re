let orRNull = x => x |> Js.Option.getWithDefault(React.null);

let mkStyle = ReactDOMRe.Style.make;

let rStr = React.string;
let rArr = React.array;
let rNull = React.null;

let setIdx = (a: array('t), index: int, value: 't): array('t) =>
  if (index < 0 || index >= Js.Array.length(a)) {
    a;
  } else {
    Array.mapi((i, x) => i == index ? value : x, a);
  };
let arrWithoutIdx = (a: array('t), index: int): array('t) => a |> Js.Array.filteri((_x, i) => i != index);
let arrEmpty = (a: array('t)) => Array.length(a) == 0;

let optEmptyStr = (x: string): option(string) => String.length(x) == 0 ? None : Some(x);

let dateFormat = "D. M. YYYY";
let dateFormatBe = "YYYY-MM-DD";

let formatDate = x => x |> MomentRe.Moment.format(dateFormat);
let formatBeDate = x => x |> MomentRe.Moment.format(dateFormatBe);

let parseDate = (x: string) => MomentRe.moment(x, ~format=[|dateFormat|]);
let parseBeDate = (x: string) => MomentRe.moment(x, ~format=[|dateFormatBe|]);

let formatWeekDay = x => x |> MomentRe.Moment.format("dd");

external unsafeJsonToObjects: Js.Json.t => Js.t({..}) = "%identity";

let classNames = (xs: list(string)) => String.concat(" ", xs);

let noop = (_x: 'a) => ();
let noop2 = (_x: 'a, _y: 'b) => ();
