open Utils;
open ReactIntl;

module Styles = {
  open Css;

  let wrapper =
    style([padding(em(0.5)), marginTop(em(1.0)), textAlign(center)]);
  let copyright = style([]);
  let madeBy = style([]);
  let gitlabLink = style([]);
  let delim = style([opacity(0.33), userSelect(none)]);
};

[@react.component]
let make = () => {
  let delim = <span className=Styles.delim> {j| ∾ |j}->rStr </span>;
  <div className=Styles.wrapper>
    {rStr({j|© 2022 GPL3|j})}
    delim
    <span className=Styles.madeBy>
      <FormattedMessage
        id="footer.madeBy"
        defaultMessage="Made in Reason and Scala by monnef."
      />
    </span>
    delim
    <a
      className=Styles.gitlabLink
      href="https://gitlab.com/monnef/obed-fe"
      target="_blank">
      "GitLab"->rStr
    </a>
  </div>;
};
