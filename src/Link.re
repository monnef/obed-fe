open Utils;
open ReactIntl;

module Styles = {
  open Css;
  open MainStyles;
  let anchor = style([]);
  noop(Colors.bgPrimary);
};

[@react.component]
let make = (~to_="", ~text="", ~textTK="", ~textDT="", ~className="", ~onClick=noop, ~children=React.null) => {
  let handleClick = evt => {
    ReactEvent.Synthetic.preventDefault(evt);
    if (to_ != "") {
      ReasonReactRouter.push(to_);
    };
    onClick();
  };
  let trPart = textTK == "" ? rNull : <FormattedMessage id=textTK defaultMessage=textDT />;
  let textPart = textTK == "" ? rStr(text) : rNull;
  <a href=to_ onClick=handleClick className={classNames([Styles.anchor, className])}> textPart trPart children </a>;
};
