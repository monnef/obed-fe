let pageLocale =
  [@intl.messages]
  {
    "fieldNameLabel": {
      "defaultMessage": "Username",
      "id": "loginForm.field.user.label",
    },
    "fieldNameErrorMissing": {
      "defaultMessage": "Username is required",
      "id": "loginForm.field.name.error.missing",
    },
    "fieldPasswordLabel": {
      "defaultMessage": "Password",
      "id": "loginForm.field.password.label",
    },
    "fieldPasswordErrorMissing": {
      "defaultMessage": "Password is missing",
      "id": "loginForm.field.password.error.missing",
    },
  };
