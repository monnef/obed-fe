open Data;
open Utils;

module Styles = {
  open Css;
  let caption = style([display(inlineBlock), margin(em(0.0))]);
  let smallNote = style([marginLeft(em(0.5))]);
  let weekDay = style([marginRight(em(0.5))]);
};

[@react.component]
let make = (~dayPlan: dayPlan, ~showDeleteButton: bool=false, ~onDelete: dayPlan => unit=noop) => {
  let parsedDate = dayPlan.date |> parseBeDate;
  <div key={dayPlan.id->Belt.Option.getWithDefault(0) |> string_of_int}>
    <div>
      <span className=Styles.weekDay> {parsedDate |> formatWeekDay |> (x => x ++ ":" |> rStr)} </span>
      <h3 className=Styles.caption> {dayPlan.caption |> rStr} </h3>
      {dayPlan.smallNote
       |> Js.Option.getWithDefault("")
       |> (x => <span className=Styles.smallNote> {rStr(x)} </span>)}
      {showDeleteButton
         ? <button type_="button" onClick={_evt => onDelete(dayPlan)}> {rStr({j|×|j})} </button> : rNull}
    </div>
    <div> {parsedDate |> formatDate |> rStr} </div>
    <div> {dayPlan.note |> Js.Option.getWithDefault("") |> rStr} </div>
    <ul>
      {dayPlan.toBuyList
       |> Array.mapi((i, x: toBuyListItem) =>
            <li key={x.id->Belt.Option.getWithDefault(- i - 1) |> string_of_int}> {x.value |> rStr} </li>
          )
       |> React.array}
    </ul>
    <div style={mkStyle(~display="none", ())}> {Js.Json.stringifyAny(dayPlan) |> Js.Option.getExn |> rStr} </div>
  </div>;
};
