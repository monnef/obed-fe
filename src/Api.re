open Axios;
open Axios_types;
open Utils;
include Js.Promise;

type auth = Axios_types.auth;

let inst = Instance.create(makeConfig(~baseURL=Config.apiPrefix, ()));

type authInfo = {
  username: string,
  password: string,
};
type myConfig = {auth: authInfo};

external convConfig: myConfig => config = "%identity";
external convAuth: authInfo => auth = "%identity";

let genConfig = (a: authInfo): config => convConfig({auth: a});

type wrappedResponse('a, 'b, 'decData) = {
  response: response('a, 'b),
  data: 'decData,
};

let decodeAndWrap =
    (decoder: Js.Json.t => 'decData, resp: response('a, 'b))
    : wrappedResponse('a, 'b, 'decData) => {
  response: resp,
  data: decoder(resp##data),
};

let decodeAndWrapP =
    (decoder: Js.Json.t => 'decData, resp: response('a, 'b))
    : Js.Promise.t(wrappedResponse('a, 'b, 'decData)) => {
  resolve(decodeAndWrap(decoder, resp));
};

type cresp('a, 'b, 'decData) =
  Js.Promise.t(wrappedResponse('a, 'b, 'decData));

//let getCategories = (): cresp(_, _, array(Data.category)) =>
//  inst->Instance.get("/categories") |> then_(decodeAndWrapP(Data.decodeCategories));
//
//let getCategory = (id: string): cresp(_, _, Data.categoryDetial) =>
//  inst->Instance.get("/categories/" ++ id) |> then_(decodeAndWrapP(Data.decodeCategoryDetail));

let getPlans = (): cresp(_, _, array(Data.plan)) =>
  inst->Instance.get("/plans") |> then_(decodeAndWrapP(Data.decodePlans));

let createTest = (p: Data.toBuyListItem) =>
  inst->Instance.postData(
    "/plans",
    p |> Data.encodeToBuyListItem |> unsafeJsonToObjects,
  );

let createPlan = (p: Data.plan, a: authInfo) =>
  inst->Instance.postDatac(
    "/plans",
    p |> Data.encodePlan |> unsafeJsonToObjects,
    genConfig(a),
  );

let getUser = (a: authInfo): cresp(_, _, Data.loggedInUser) =>
  inst->Instance.getc("/user/current", genConfig(a))
  |> then_(decodeAndWrapP(Data.decodeLoggedInUser));
