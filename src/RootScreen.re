open Utils;
open ReactIntl;

[@react.component]
let make = () => {
  <div> <FormattedMessage id="root.text" defaultMessage="Choose option from the menu." /> </div>;
};
