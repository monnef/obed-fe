open Data;
open Utils;
open ReactIntl;
open DayPlanFormLocale;

module Styles = {
  open Css;
  open MainStyles;

  let form = style([backgroundColor(Colors.bgSecondary), padding(em(0.5))]);
  let toBuyListWrapper = merge([Decoration.indentedBox, style([])]);
  let removeToBuyItemButton = style([marginLeft(em(0.5))]);
};

module DayPlanForm = {
  open Formality;

  type field =
    | Caption
    | Date
    | ToBuyList
    | ToBuyListNewItem
    | Note
    | SmallNote;

  type state = {
    caption: string,
    date: string,
    toBuyList: array(toBuyListItem),
    toBuyListNewItem: string,
    note: string,
    smallNote: string,
  };

  type message = ReactIntl.message;
  type submissionError = unit;

  module CaptionField = {
    let update = (state, value) => {...state, caption: value};

    let validator = {
      field: Caption,
      strategy: Strategy.OnFirstSuccessOrFirstBlur,
      dependents: None,
      validate: state =>
        switch (state.caption) {
        | "" => Error(pageLocale##fieldCaptionErrorMissing)
        | _ => Ok(Valid)
        },
    };
  };

  module DateField = {
    let update = (state, value) => {...state, date: value};

    let validator = {
      field: Date,
      strategy: Strategy.OnFirstSuccessOrFirstBlur,
      dependents: None,
      validate: state =>
        switch (state.date) {
        | "" => Error(pageLocale##fieldDateLabelErrorMissing)
        | d =>
          d |> parseDate |> MomentRe.Moment.isValid ? Ok(Valid) : Error(pageLocale##fieldDateLabelErrorInvalidFormat)
        },
    };
  };

  module NoteField = {
    let update = (state, value) => {...state, note: value};
  };

  module SmallNoteField = {
    let update = (state, value) => {...state, smallNote: value};
  };

  module ToBuyListField = {
    let update = (state, value) => {...state, toBuyList: value};
  };

  module ToBuyListNewItemField = {
    let update = (state, value) => {...state, toBuyListNewItem: value};
  };

  let validators = [CaptionField.validator, DateField.validator];

  let messageToIntlMessage = x => x;
};

module DayPlanFormHook = CustomForm.Make(DayPlanForm);

[@react.component]
let make = (~onSubmit: DayPlanForm.state => unit, ~onCancel: unit => unit, ~defaultDate: string) => {
  let t = ReactIntl.useIntl();
  let DayPlanFormHook.{form, renderTextInput} =
    DayPlanFormHook.useFormCustom(
      ~initialState={caption: "", date: defaultDate, toBuyList: [||], note: "", smallNote: "", toBuyListNewItem: ""},
      ~onSubmit=
        (state, submissionCallbacks) => {
          Js.logMany([|"submit", Js.Json.stringifyAny(state)->Belt.Option.getWithDefault("???")|]);
          onSubmit(state);
          submissionCallbacks.notifyOnSuccess(None);
          submissionCallbacks.reset();
        },
      ~t,
    );
  let dialog = ModalDialog.useDialog();

  let onTbiAdd = () => {
    let value = form.state.toBuyListNewItem;
    if (value != "") {
      let newItem = {value, id: None, dayPlanId: None};
      let newList = Array.append(form.state.toBuyList, [|newItem|]);
      form.change(
        ToBuyList,
        form.state->DayPlanForm.ToBuyListField.update(newList)->DayPlanForm.ToBuyListNewItemField.update(""),
      );
    };
  };
  let onTbiRemove = (i: int) => {
    dialog.show(
      {j|Opravdu odebrat položku |j} ++ "\"" ++ form.state.toBuyList->Array.get(i).value ++ "\"?",
      ModalDialogTypes.Confirm,
      button =>
      if (button == ModalDialogTypes.OkButton) {
        let newList = arrWithoutIdx(form.state.toBuyList, i);
        form.change(ToBuyList, form.state->DayPlanForm.ToBuyListField.update(newList));
      }
    );
  };
  let handleKeyPressInTBLNI = evt =>
    if (ReactEvent.Keyboard.key(evt) == "Enter") {
      ReactEvent.Synthetic.preventDefault(evt);
      ReactEvent.Synthetic.stopPropagation(evt);
      onTbiAdd();
    };
  let controlsDisabled = form.submitting;

  <form onSubmit={form.submit->Formality.Dom.preventDefault} className=Styles.form>
    {renderTextInput(
       Date,
       x => x.date,
       DayPlanForm.DateField.update,
       t->Intl.formatMessage(pageLocale##fieldDateLabel),
       (),
     )}
    {renderTextInput(
       Caption,
       x => x.caption,
       DayPlanForm.CaptionField.update,
       t->Intl.formatMessage(pageLocale##fieldCaptionLabel),
       (),
     )}
    {renderTextInput(
       SmallNote,
       x => x.smallNote,
       DayPlanForm.SmallNoteField.update,
       t->Intl.formatMessage(pageLocale##fieldSmallNoteLabel),
       (),
     )}
    {renderTextInput(
       Note,
       x => x.note,
       DayPlanForm.NoteField.update,
       t->Intl.formatMessage(pageLocale##fieldNoteLabel),
       (),
     )}
    <div className=Styles.toBuyListWrapper>
      <ul>
        {form.state.toBuyList
         |> Array.mapi((i, x) =>
              <li key={i->string_of_int}>
                {x.value |> rStr}
                <button type_="button" className=Styles.removeToBuyItemButton onClick={_evt => onTbiRemove(i)}>
                  {rStr({j|×|j})}
                </button>
              </li>
            )
         |> rArr}
      </ul>
      {renderTextInput(
         ToBuyListNewItem,
         x => x.toBuyListNewItem,
         DayPlanForm.ToBuyListNewItemField.update,
         t->Intl.formatMessage(pageLocale##fieldNewItemLabel),
         ~onKeyPress=handleKeyPressInTBLNI,
         (),
       )}
      <button disabled=controlsDisabled onClick={_evt => onTbiAdd()} type_="button">
        <FormattedMessage id="dayPlanForm.addItemButton" defaultMessage="Add item" />
      </button>
    </div>
    <button disabled=controlsDisabled onClick={_evt => onCancel()} type_="button">
      <FormattedMessage id="dayPlanForm.cancelButton" defaultMessage="Cancel" />
    </button>
    <button disabled=controlsDisabled type_="submit">
      {form.submitting
         ? <FormattedMessage id="dayPlanForm.createDayPlanButton.submitting" defaultMessage="Submitting..." />
         : <FormattedMessage id="dayPlanForm.createDayPlanButton" defaultMessage="Create day plan" />}
    </button>
  </form>;
};
