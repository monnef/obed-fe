open Css;

module Colors = {
  let bgPrimary = rgb(250, 250, 255);
  let bgSecondary = rgb(200, 250, 255);
  let bgDark = rgb(128, 128, 128);
  let link = rgb(256, 0, 128);
  let linkSecondary = rgb(256, 50, 180);
  let linkBg = rgb(256, 256, 200);
  let linkBgSec = rgb(255, 255, 255);
  let error = rgb(240, 50, 50);
};

module Fonts = {
  let hack =
    fontFace(
      ~fontFamily="hack",
      ~src=[url("assets/Hack-Regular.woff2")],
      ~fontWeight=`normal,
      ~fontStyle=`normal,
      (),
    );
  let ffHack = fontFamily(hack);
};

module Decoration = {
  let indentedBox =
    style([
      borderLeft(em(0.2), solid, Colors.bgDark),
      paddingLeft(em(0.5)),
      marginBottom(em(1.0)),
    ]);
};

global(
  "body",
  [backgroundColor(Colors.bgPrimary), Fonts.ffHack, boxSizing(borderBox)],
);
global("input", [Fonts.ffHack]);
global("input[type='text']", [height(em(2.0))]);
global(
  "a",
  [
    color(Colors.link),
    textDecoration(none),
    padding2(~h=em(0.2), ~v=em(0.1)),
  ],
);
global(
  "a:hover",
  [color(Colors.linkSecondary), backgroundColor(Colors.linkBgSec)],
);
global(
  "button",
  [
    height(em(3.0)),
    padding2(~v=zero, ~h=em(0.8)),
    borderRadius(em(0.5)),
    borderWidth(em(0.2)),
    borderColor(Colors.bgDark),
    borderStyle(solid),
    backgroundColor(Colors.linkBg),
    Fonts.ffHack,
    color(Colors.link),
    cursor(`pointer),
    fontSize(em(1.0)),
  ],
);
global("button[disabled]", [color(Colors.bgDark)]);
global(
  "button:hover",
  [color(Colors.linkSecondary), backgroundColor(Colors.linkBgSec)],
);
let smallButton = style([padding(em(0.2)), height(auto)]);

let inputBase =
  style([
    display(block),
    margin2(~v=em(1.0), ~h=em(0.)),
    padding2(~v=em(0.1), ~h=em(0.2)),
  ]);
