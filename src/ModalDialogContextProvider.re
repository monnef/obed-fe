open ModalDialogTypes;

let modalDialogContext: React.Context.t(modalDialogContextT) =
  React.createContext({shown: false, show: (_, _, _) => (), hide: () => ()});

let makeProps = (~children, ~value, ()) => {"children": children, "value": value};

let make = React.Context.provider(modalDialogContext);
