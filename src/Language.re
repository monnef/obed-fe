type language =
  | En
  | Cs;

let langToStr = lang =>
  switch (lang) {
  | En => "en"
  | Cs => "cs"
  };

let strToLang = str =>
  switch (str) {
  | "en" => Some(En)
  | "cs" => Some(Cs)
  | _ => None
  };
