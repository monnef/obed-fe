open Data;
open Utils;
open ReactIntl;
open LoginFormLocale;

module Styles = {
  open Css;
  open MainStyles;

  let form =
    style([backgroundColor(Colors.bgSecondary), padding(em(0.5))]);
};

module LoginForm = {
  open Formality;

  type field =
    | Name
    | Password;

  type state = {
    name: string,
    password: string,
  };

  type message = ReactIntl.message;
  type submissionError = unit;

  module NameField = {
    let update = (state, value) => {...state, name: value};

    let validator = {
      field: Name,
      strategy: Strategy.OnFirstSuccessOrFirstBlur,
      dependents: None,
      validate: state =>
        switch (state.name) {
        | "" => Error(pageLocale##fieldNameErrorMissing)
        | _ => Ok(Valid)
        },
    };
  };

  module PasswordField = {
    let update = (state, value) => {...state, password: value};

    let validator = {
      field: Password,
      strategy: Strategy.OnFirstSuccessOrFirstBlur,
      dependents: None,
      validate: state =>
        switch (state.password) {
        | "" => Error(pageLocale##fieldPasswordErrorMissing)
        | _ => Ok(Valid)
        },
    };
  };

  let validators = [NameField.validator, PasswordField.validator];

  let messageToIntlMessage = x => x;
};

module LoginFormHook = CustomForm.Make(LoginForm);

[@react.component]
let make = (~onSubmit: LoginForm.state => unit) => {
  let t = ReactIntl.useIntl();
  let LoginFormHook.{form, renderTextInput} =
    LoginFormHook.useFormCustom(
      ~initialState={name: "", password: ""},
      ~onSubmit=
        (state, submissionCallbacks) => {
          Js.logMany([|
            "submit",
            Js.Json.stringifyAny(state)->Belt.Option.getWithDefault("???"),
          |]);
          onSubmit(state);
          submissionCallbacks.notifyOnSuccess(None);
          submissionCallbacks.reset();
        },
      ~t,
    );
  let controlsDisabled = form.submitting;

  <form
    onSubmit={form.submit->Formality.Dom.preventDefault} className=Styles.form>
    {renderTextInput(
       Name,
       x => x.name,
       LoginForm.NameField.update,
       t->Intl.formatMessage(pageLocale##fieldNameLabel),
       (),
     )}
    {renderTextInput(
       Password,
       x => x.password,
       LoginForm.PasswordField.update,
       t->Intl.formatMessage(pageLocale##fieldPasswordLabel),
       ~type_=TextInputPassword,
       (),
     )}
    <button disabled=controlsDisabled type_="submit">
      {form.submitting
         ? <FormattedMessage
             id="loginForm.loginButton.submitting"
             defaultMessage="Submitting..."
           />
         : <FormattedMessage
             id="loginForm.loginButton"
             defaultMessage="Log in"
           />}
    </button>
  </form>;
};
