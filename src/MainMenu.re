open Utils;
open Language;
open Data;
open ReactIntl;

module Styles = {
  open Css;

  let wrapper =
    style([
      padding(em(0.5)),
      display(flexBox),
      marginBottom(em(1.0)),
      flexWrap(wrap),
    ]);
  let link = style([]);
  let linkWrapper = style([margin(em(0.3))]);
  let langSelector =
    style([display(flexBox), alignItems(center), justifyContent(flexEnd)]);
  let langBut =
    merge([MainStyles.smallButton, style([marginLeft(em(0.33))])]);
  let activeLangBut =
    merge([
      langBut,
      style([backgroundColor(MainStyles.Colors.bgSecondary)]),
    ]);
  let userWrapper =
    style([
      flexGrow(2.0),
      display(flexBox),
      alignItems(center),
      justifyContent(flexEnd),
      marginRight(em(1.)),
    ]);
  let notLoggedIn =
    style([
      backgroundColor(MainStyles.Colors.error),
      color(MainStyles.Colors.bgPrimary),
      margin(em(0.3)),
      padding2(~v=em(0.1), ~h=em(0.3)),
      borderRadius(em(0.2)),
    ]);
};

[@react.component]
let make =
    (
      ~onChangeLanguage: language => unit,
      ~activeLanguage: language,
      ~availableLanguages: array(language),
      ~user: option(loggedInUser),
    ) => {
  let links = [|
    ("/plans", "mainMenu.planList", "Plans list"),
    ("/new-plan", "mainMenu.newPlan", "New plan"),
  |];
  let renderLang = x =>
    <button
      className=Styles.(x == activeLanguage ? activeLangBut : langBut)
      onClick={_e => onChangeLanguage(x)}
      key={x->langToStr}>
      {x->langToStr->rStr}
    </button>;

  let userPart =
    <div className=Styles.userWrapper>
      {switch (user) {
       | Some(u) => u.name->rStr
       | None =>
         <Link className=Styles.notLoggedIn to_="/login">
           <FormattedMessage
             id="mainMenu.notLoggedIn"
             defaultMessage="Not logged in"
           />
         </Link>
       }}
    </div>;

  <div className=Styles.wrapper>
    <Link to_="/" text={j|🏠|j} />
    {links
     |> Array.map(((trg, key, def)) =>
          <span className=Styles.linkWrapper key>
            "["->rStr
            <Link to_=trg textTK=key textDT=def className=Styles.link key />
            "]"->rStr
          </span>
        )
     |> rArr}
    userPart
    <div className=Styles.langSelector>
      {availableLanguages |> Array.map(renderLang) |> rArr}
    </div>
  </div>;
};
