open Utils;
open ReactIntl;

[@react.component]
let make =
    (~user: option(Data.loggedInUser), ~onFormFilled: Api.authInfo => unit) => {
  let handleFormSubmit = (x: LoginForm.LoginForm.state) => {
    Js.log2("handleFormSubmit", x);
    onFormFilled({username: x.name, password: x.password});
  };
  <div>
    <h2>
      <FormattedMessage id="loginScreen.caption" defaultMessage="Login" />
    </h2>
    {switch (user) {
     | None => <LoginForm onSubmit=handleFormSubmit />
     | Some(_) =>
       <FormattedMessage
         id="loginScreen.alreadyLoggedIn"
         defaultMessage="Already logged in"
       />
     }}
  </div>;
};
