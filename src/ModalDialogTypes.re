type buttonType =
  | OkButton
  | CancelButton;

type dialogMode =
  | Confirm
  | Info;

type callbackT = buttonType => unit;

type modalDialogContextT = {
  shown: bool,
  show: (string, dialogMode, callbackT) => unit,
  hide: unit => unit,
};
