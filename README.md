# Obed FE

Status: **alpha**  
Only most important functionality is implemented (e.g. creation of plans, day plans and "to buy" list items; no updating of existing items).

# Setup

* install [Node](https://nodejs.org) (see `.node-version` for a recommended version)
* `npm i`
* copy `src/Config.sample.re` to `src/Config.re`, update as necessary

# Development

* start `npm start`

# Build

`npm run build:prod`

Everything necessary will be located at `dist` directory.

# Build and ~~run~~ serve

`npm run prod`

It will clean, compile Reason files, package them and then start a web server (URLs are printed in console, e.g. http://127.0.0.1:8000).

## TODO
* saving auth to local storage or cookie
* saving language
* showing errors to user
* deleting plans (with confirm dialog)
* plan edit
