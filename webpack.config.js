const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const distPath = path.join(__dirname, 'dist');

module.exports = {
    entry: './lib/js/src/Index.bs.js',
    // If you ever want to use webpack during development, change 'production'
    // to 'development' as per webpack documentation. Again, you don't have to
    // use webpack or any other bundler during development! Recheck README if
    // you didn't know this
    mode: 'production',
    devtool: 'source-map',
    output: {
        path: distPath,
        filename: 'index.js',
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: 'indexProduction.html', to: distPath + '/index.html' },
                { from: 'assets', to: distPath + '/assets' },
            ],
        }),
    ],
};
