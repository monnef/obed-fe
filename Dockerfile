FROM caddy:2.4.6

COPY docker/Caddyfile /etc/caddy/Caddyfile
COPY dist /usr/share/caddy
